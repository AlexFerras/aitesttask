#include <iostream>
#include <unordered_map>

using namespace std;

class IntSum
{
private:
    std::unordered_map<std::string, int> Ints;
    
public:
    IntSum()
    {
    }
    
    void AddValue(std::string Key, int Value)
    {
        Ints[Key] = Value;
    }
    
    bool RemoveValue(std::string Key)
    {
        if (Ints.count(Key) == 0)
            return false;
        Ints.erase(Key);
        return true;
    }
    
    int GetTotal()
    {
        int s = 0;
        for (auto i : Ints)
        {
            s+=i.second;
        }
        return s;
    }
    
};

void Run()
{
    IntSum ints = IntSum();
    while (true)
    {
        std::string command;
        std::cout << "Enter command:\nAvailable commands:\n /add - add value\n /remove - remove value\n /total - print running total of values.\n";
        std::cin >> command;
        
        if (command == "/add")
        {
            cout << "Enter key.\n";
            string key;
            cin >> key;
            cout << "Enter value.\n";
            int value;
            cin >> value;
            ints.AddValue(key, value);
            cout << "Value added." << "\n";
        }
        else if (command == "/remove")
        {
            cout << "Enter key.\n";
            string key;
            cin >> key;
            bool success = ints.RemoveValue(key);
            if (success)
                cout << "Value removed." << "\n";
            else
                cout << "Value not found." << "\n";
        }
        else if (command == "/total")
        {
            cout << ints.GetTotal() << "\n";
        }
        else
        {
            cout << "Incorrect command.";
        }
    }
}


int main() 
{
    Run();
    return 0;
}